export class Item{
	constructor(title, id){
		this.title = title;
		this.id = id;
		this.done = false;
	}

	displayTitle(){
		console.log(this.title);
	}

	insertHMTL(key){
		const markup = `
			<li class="list-group-item list-group-item-info">
		  			<div class="row">
			  			<div class="col-md-10"><h3 class="">${this.title} </h3></div>
			  			<div class="col-md-2">
				  			<div class="form-check">
							  <input class="form-check-input position-static float-right big-ck" type="checkbox" id="blankCheckbox" value="option1" data-id=${this.id}>
							</div>
						</div>
					</div>
		  		</li>
		`;

		document.querySelector('#todo-list').innerHTML += markup;
	}
}