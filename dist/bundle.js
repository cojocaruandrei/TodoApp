/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Item.js":
/*!*********************!*\
  !*** ./src/Item.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n\tvalue: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Item = exports.Item = function () {\n\tfunction Item(title, id) {\n\t\t_classCallCheck(this, Item);\n\n\t\tthis.title = title;\n\t\tthis.id = id;\n\t\tthis.done = false;\n\t}\n\n\t_createClass(Item, [{\n\t\tkey: 'displayTitle',\n\t\tvalue: function displayTitle() {\n\t\t\tconsole.log(this.title);\n\t\t}\n\t}, {\n\t\tkey: 'insertHMTL',\n\t\tvalue: function insertHMTL(key) {\n\t\t\tvar markup = '\\n\\t\\t\\t<li class=\"list-group-item list-group-item-info\">\\n\\t\\t  \\t\\t\\t<div class=\"row\">\\n\\t\\t\\t  \\t\\t\\t<div class=\"col-md-10\"><h3 class=\"\">' + this.title + ' </h3></div>\\n\\t\\t\\t  \\t\\t\\t<div class=\"col-md-2\">\\n\\t\\t\\t\\t  \\t\\t\\t<div class=\"form-check\">\\n\\t\\t\\t\\t\\t\\t\\t  <input class=\"form-check-input position-static float-right big-ck\" type=\"checkbox\" id=\"blankCheckbox\" value=\"option1\" data-id=' + this.id + '>\\n\\t\\t\\t\\t\\t\\t\\t</div>\\n\\t\\t\\t\\t\\t\\t</div>\\n\\t\\t\\t\\t\\t</div>\\n\\t\\t  \\t\\t</li>\\n\\t\\t';\n\n\t\t\tdocument.querySelector('#todo-list').innerHTML += markup;\n\t\t}\n\t}]);\n\n\treturn Item;\n}();\n\n//# sourceURL=webpack:///./src/Item.js?");

/***/ }),

/***/ "./src/app.js":
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _Item = __webpack_require__(/*! ./Item.js */ \"./src/Item.js\");\n\nvar tasks = [];\n\nvar newTaskBtn = document.getElementsByClassName('newTask')[0];\nnewTaskBtn.addEventListener('click', function () {\n\tvar index = tasks.length;\n\tvar checkbox = null;\n\ttasks.push(new _Item.Item('Task ' + (index + 1), index));\n\ttasks[index].insertHMTL();\n\n\tcheckbox = document.querySelectorAll(\"input[type='checkbox'][data-id='\" + index + \"']\")[0];\n\tcheckbox.addEventListener('click', function () {\n\t\tconsole.log('yolo');\n\t});\n});\n\n// for(let [index, val] of tasks.entries()){\n// \tval.id = index;\n// \tval.insertHMTL();\n// }\n\n// let checkboxes = document.querySelectorAll(\".form-check-input\");\n// for(let [index, val] of checkboxes.entries()){\n// \tval.addEventListener('click', () => {\n// \t\tconsole.log(arr[index]);\n// \t});\n// }\n\n//# sourceURL=webpack:///./src/app.js?");

/***/ })

/******/ });